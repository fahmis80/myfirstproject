const dataArrayTodoList = [
	"Belajar javascript metode DOM",
	"belajar javascript metode api",
	"belajar javascript metode dasar"
];

function removeDoubleData(){
	const dataListTodo = document.getElementById('dataListTodo');
	while(dataListTodo.firstChild){
		dataListTodo.removeChild(dataListTodo.firstChild);
	}
}

function doneButton(dataIndex){
	dataArrayTodoList.splice(dataIndex, 1);
	displayDataTodo();
}

function addListView(dataIndex, arrayList){
	const trTodo = document.createElement('tr');
	const tdTodo = document.createElement('td');
	trTodo.appendChild(tdTodo);

	const buttonList = document.createElement('input');
	buttonList.type = 'submit';
	buttonList.value = 'Done';
	tdTodo.appendChild(buttonList);

	buttonList.onclick = function(){
		doneButton(dataIndex);
	}

	const tdData = document.createElement('td');
	tdData.textContent = arrayList;
	trTodo.append(tdData);


	const dataListTodo = document.getElementById('dataListTodo');
	dataListTodo.appendChild(trTodo);
	console.info('berhasil mengeluarkan data');
}

function displayDataTodo(){
	removeDoubleData();
	for (let dataIndex = 0; dataIndex < dataArrayTodoList.length; dataIndex++) {
		const arrayList = dataArrayTodoList[dataIndex];
		
		const search = document.getElementById('searchTodo').value.toLowerCase();
		if(arrayList.toLowerCase().includes(search)){
			addListView(dataIndex, arrayList)
		}
	}
}

document.forms['todolist'].onsubmit = function(event){
	event.preventDefault();
	const inputTodo = document.getElementById('inputTodo').value;

	dataArrayTodoList.push(inputTodo);
	document.forms['todolist'].reset();
	console.info(dataArrayTodoList);
	displayDataTodo();
}
const search = document.getElementById('searchTodo');
search.onkeyup = function(){
	displayDataTodo();
}
search.onkeydown = function(){
	displayDataTodo();
}
displayDataTodo();