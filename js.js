const arrayData = [
	"belajar javascript module",
	"belajar javascript oop",
	"beajar javascript array"
];

function clearTodoList(){
	const clear = document.getElementById('todoListBody');
	while(clear.firstChild){
		clear.removeChild(clear.firstChild);
	}
}

function removeTodoList(dataObjek){
	arrayData.splice(dataObjek, 1);
	displayTodoList();
}

function addDataList(dataObjek, objekDataArray){
		const tr = document.createElement('tr');
		const tdButton = document.createElement('td');
		tr.appendChild(tdButton);

		const buttonDone = document.createElement('input');
		buttonDone.type = 'submit';
		buttonDone.value = 'Done';
		buttonDone.onclick = function(){
			removeTodoList(dataObjek)
		}
		tdButton.appendChild(buttonDone);

		const getData = document.createElement('td');
		getData.textContent = objekDataArray;
		tr.appendChild(getData);

		const todoListBody = document.getElementById('todoListBody');
		todoListBody.appendChild(tr);
}

function displayTodoList(){
	clearTodoList();
	for(let dataObjek = 0; dataObjek < arrayData.length; dataObjek++){
		const objekDataArray = arrayData[dataObjek];

		const searData = document.getElementById('search').value.toLowerCase();
		if (objekDataArray.toLowerCase().includes(searData)) {
			addDataList(dataObjek, objekDataArray);
		}
	}
}
document.forms['todoList'].onsubmit = function(event){
	event.preventDefault();

	const inputTodo = document.getElementById('inputTodo').value;
	arrayData.push(inputTodo);
	
	document.forms['todoList'].reset();
	console.info(arrayData);
	displayTodoList()
}

const searchData = document.getElementById('search')
searchData.onkeyup = function(){
	displayTodoList();
}
searchData.onkeydown = function(){
	displayTodoList();
}
displayTodoList();